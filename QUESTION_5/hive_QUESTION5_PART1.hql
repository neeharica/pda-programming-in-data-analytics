drop table if exists distmeas;

-- create table distinct_phone1, and read all the lines in '/user/inputs', this is the path on your local HDFS
create external table if not exists distmeas
(Pid string,
hname string,
haddress string,
hcity string,
hstate string,
hcode string,
hcountyname string,
hphonenum string,
hcahpsmeasID string,
hcahpsquest string,
hcahpsansdes string,
patientsurvstarrat string,
patientsurvstarratfn string,
hcahpsansper string,
hcahpsansperfn string,
hcahpslinmeanval string,
numofcompsurv string,
numofcompsurvfn string,
survresprateper string,
survresprateperfn string,
meastartdate string,
meaenddate string,
hoslocation string)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY ';'
lines terminated by '\n'
stored as textfile
location '/user/hduser/inputs/';

drop table if exists intermediate_data;

-- create table word_count, this is the output table which will be put in '/user/outputs' as a text file, this is the path on your local HDFS

create external table if not exists intermediate_data(
word string,
data string)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
lines terminated by '\n' 
STORED AS TEXTFILE 
LOCATION '/user/hduser/outputs1/';


insert overwrite table intermediate_data (SELECT Pid, CONCAT(CONCAT("(", CONCAT_WS("., ", hcahpsansdes, survresprateper)), ")") as hcahpdata FROM distmeas where survresprateper != '0');


drop table if exists word_count1;

-- create table word_count, this is the output table which will be put in '/user/outputs' as a text file, this is the path on your local HDFS

create external table if not exists word_count1(
word string,
count string)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
lines terminated by '\n' 
STORED AS TEXTFILE 
LOCATION '/user/hduser/outputs/';

-- add the mapper&reducer scripts as resources, please change your/local/path
add file /usr/local/hadoop/hive_mapper_QUESTION5.py;
add file /usr/local/hadoop/hive_reducer_QUESTION5_PART1.py;

from (
from intermediate_data
map word, data
using './hive_mapper_QUESTION5.py'
as word, count
cluster by word) map_output
insert overwrite table word_count1
reduce map_output.word, map_output.count
using './hive_reducer_QUESTION5_PART1.py'
as word,count;

