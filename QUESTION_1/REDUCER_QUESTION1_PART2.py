#!/usr/bin/python

import sys

distKeyAnsPerc = {}
distAns = set()
distHosAns = {}


for line in sys.stdin:
    data_mapped = line.strip().split(".,")
    if len(data_mapped) != 2:
        # Something has gone wrong. Skip this line.
        continue

    curKey, curData = data_mapped

    curData = curData.strip().split(";")
    if len(curData) != 2:
        # Something has gone wrong. Skip this line.
        continue

    curAns, curPerc = curData
    
        
    if curKey not in distKeyAnsPerc:
        distKeyAnsPerc[int(curKey)] = {}
     
    distKeyAnsPerc[int(curKey)][curAns] = distKeyAnsPerc[int(curKey)].get(curAns, 0) + int(curPerc)
    distKeyAnsPerc.update({curKey: {curAns: distKeyAnsPerc[int(curKey)][curAns]}})

    if (int(curPerc) >= 1) and (int(curPerc) <= 30):
        if curKey not in distHosAns:
            distHosAns[int(curKey)] = {}
        distHosAns[int(curKey)][int(curPerc)] = distHosAns[int(curKey)].get(int(curPerc), "") +"------"+ curAns
        distHosAns.update({curKey: {int(curPerc): distHosAns[int(curKey)][int(curPerc)]}})
    
    #distAns.add(int(curPerc))

    #distHosAns[curAns] = distHosAns.get(curAns, 0) + int(curPerc)


print("HOSPITAL ID, HCAHPS PERCENT, HCAHPS ANSWER ")
print("===========  ==============  ============= ")
for key in sorted(distHosAns.keys()):
    dt = distHosAns[key]
    for ques in sorted(dt.keys()):
        print(key, "::", ques,"==", dt[ques]) 
    


