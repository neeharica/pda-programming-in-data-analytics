HCAHPS MEASURE ID, NUMBER OF MEAN VALUES, SUM OF VALUES, MEAN OF VALUES
=================  =====================  =============  ==============
H_CLEAN_LINEAR_SCORE,			2594,			223927,			86.0 
H_COMP_1_LINEAR_SCORE,			991,			87802,			88.0 
H_COMP_2_LINEAR_SCORE,			882,			78345,			88.0 
H_COMP_3_LINEAR_SCORE,			2821,			240492,			85.0 
H_COMP_5_LINEAR_SCORE,			1481,			122286,			82.0 
H_COMP_6_LINEAR_SCORE,			2826,			245294,			86.0 
H_COMP_7_LINEAR_SCORE,			2978,			246489,			82.0 
H_HSP_RATING_LINEAR_SCORE,			2484,			217146,			87.0 
H_QUIET_LINEAR_SCORE,			3324,			281405,			84.0 
H_RECMND_LINEAR_SCORE,			4490,			388906,			86.0 
HCAHPS MEASURE ID, TOP 3 MEAN OF VALUES
=================  ====================
['H_COMP_1_LINEAR_SCORE', 'H_COMP_2_LINEAR_SCORE'],               88.0 
['H_HSP_RATING_LINEAR_SCORE'],               87.0 
