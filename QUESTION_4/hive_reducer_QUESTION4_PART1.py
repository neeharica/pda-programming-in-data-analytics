#!/usr/bin/python
import sys


distKeyPatPer = {}
distKeyCount = {}
distKeyCountDisp = {}

for line in sys.stdin:
    line = line.strip()
    line = line.strip("()")
    words= line.split("\t")
    curKey, curData = words

    data_key, data_ans = curData.split(".,")
    data_ans = data_ans.strip()
    
    if curKey not in distKeyPatPer:
        distKeyPatPer[curKey] = {}
    if data_key not in distKeyPatPer[curKey]:
        distKeyPatPer[curKey][data_key] = set()

    #distKeyPatPer[int(curKey)][data_key] = distKeyPatPer[int(curKey)].get(data_key, 0) + int(data_ans)
    #distKeyPatPer[curKey] = distKeyPatPer.get(curKey, "") + curData
    
    (distKeyPatPer[curKey].get(data_key, "")).add(data_ans)
    distKeyCount[curKey] = distKeyCount.get(curKey, 0) + int(data_ans)
    #(distKeyPatPer[curKey].get(data_key, "")).append(data_ans)
    #(distKeyPatPer.get(curKey, "")).append(curData)
    #distKeyPatPer.update({int(curKey): {data_key: distKeyPatPer[int(curKey)][data_key]}})
        
for key in sorted(distKeyPatPer.keys()):
    data = distKeyPatPer[key]
    for datakey in sorted(data.keys()):
        if distKeyCount[key] not in distKeyCountDisp:
            distKeyCountDisp[distKeyCount[key]] = {}
        if key not in distKeyCountDisp[distKeyCount[key]]:
            distKeyCountDisp[distKeyCount[key]][key] = {}
        distKeyCountDisp[distKeyCount[key]][key][datakey] = distKeyCountDisp[distKeyCount[key]][key].get(datakey, "") + str(data[datakey])
        #distKeyCountDisp.update({distKeyCount[key]: {key: {datakey: distKeyCountDisp[distKeyCount[key]][key][datakey]} }})

for key in sorted(distKeyCountDisp.keys()):
    datahospkey = distKeyCountDisp[key]
    for data_key in sorted(datahospkey.keys()):
        queshospkey = datahospkey[data_key]
        for per_key in sorted(queshospkey.keys()):
            if (key >= 10) and (key <= 30):
                print '(count:%s)==>(hospID:%s)[==]%s[==]\t%s' % ( key, data_key, per_key, queshospkey[per_key])  

#for key in sorted(distKeyPatPer.keys()):
    #print '%s==\t%s' % ( key, distKeyPatPer[key])
#    data = distKeyPatPer[key]
#    for datakey in sorted(data.keys()):
#        if (distKeyCount[key] >= 10) and (distKeyCount[key] <= 30):
#            print '%s(count:%s)==%s\t%s' % ( key, distKeyCount[key], datakey,data[datakey])


