#!/usr/bin/python

import sys


distMeasIDMean = {}
distHCAPTop = {}


for line in sys.stdin:
    data_mapped = line.strip().split(".,")
    if len(data_mapped) != 2:
        # Something has gone wrong. Skip this line.
        continue

    curKey, curData = data_mapped

    inter_data = curData.strip().split(";")
    if len(inter_data) != 3:
        # Something has gone wrong. Skip this line.
        continue

    curMeanVal, curMeasID, curAdd = inter_data

    #print(inter_data)
    
    if curMeasID not in distMeasIDMean:
        distMeasIDMean[curMeasID] = []
    distMeasIDMean.get(curMeasID, 0).append(int(curMeanVal))
    distMeasIDMean.update({curMeasID: distMeasIDMean[curMeasID]})

    #print(distMeasIDMean[curMeasID])
        
     

print("HCAHPS MEASURE ID, NUMBER OF MEAN VALUES, SUM OF VALUES, MEAN OF VALUES")
print("=================  =====================  =============  ==============")
for key in sorted(distMeasIDMean.keys()):
    meanval = distMeasIDMean[key]
    mval = 0
    print "{0},\t\t\t{1},\t\t\t{2},\t\t\t{3} ".format(key, len(meanval), sum(meanval), float((sum(meanval))/(len(meanval))))
    mval = float((sum(meanval))/(len(meanval)))
    if mval not in distHCAPTop:
        distHCAPTop[mval] = []
    distHCAPTop.get(mval, "").append(key)
    distHCAPTop.update({mval: distHCAPTop[mval]})
    

print("HCAHPS MEASURE ID, TOP 3 MEAN OF VALUES")
print("=================  ====================")
count = 0
for key in sorted(distHCAPTop.keys()):
    mid = distHCAPTop[key]
    count += len(mid)
    print "{0},               {1} ".format(mid, key)
    if count >= 3:
        break






