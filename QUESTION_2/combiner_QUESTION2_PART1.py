#!/usr/bin/python

import sys

distKeyMean = {}
distAns = set()
distHosAns = {}


for line in sys.stdin:
    data_mapped = line.strip().split(".,")
    if len(data_mapped) != 2:
        # Something has gone wrong. Skip this line.
        continue

    curKey, curData = data_mapped

    inter_data = curData.strip().split(";")
    if len(inter_data) != 3:
        # Something has gone wrong. Skip this line.
        continue

    curAdd, curMeasID, curMeanVal = inter_data

    #print(inter_data)

    if (int(curMeanVal) >= 80) and (int(curMeanVal) <= 90):
        
        if curKey not in distKeyMean:
            distKeyMean[int(curKey)] = {}
        if int(curMeanVal) not in distKeyMean[int(curKey)]:
            distKeyMean[int(curKey)][int(curMeanVal)] = {}
     
        distKeyMean[int(curKey)][int(curMeanVal)][curMeasID] = distKeyMean[int(curKey)][int(curMeanVal)].get(curMeasID, " ") + curAdd
        distKeyMean.update({curKey: {int(curMeanVal): {curMeasID: distKeyMean[int(curKey)][int(curMeanVal)][curMeasID]}}})

    

for key in sorted(distKeyMean.keys()):
    dt = distKeyMean[key]
    for keymeanval in sorted(dt.keys()):
        ans = dt[keymeanval]
        for keymeasID in sorted(ans.keys()):
            print "{0}.,{1};{2};{3} ".format(key, keymeanval, keymeasID, ans[keymeasID])
            #print(key, "::::", keymeanval,"====", keymeasID, "++++", ans[keymeasID])    
    




