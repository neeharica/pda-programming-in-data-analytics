
HospSurv = LOAD 'hdfs://localhost:54310/data/hospital_patient_survey_1.csv' USING PigStorage(';') AS (Pid:chararray, hname:chararray, haddress:chararray, hcity:chararray, hstate:chararray, hcode:chararray, hcountyname:chararray, hphonenum:chararray, hcahpsmeasID:chararray, hcahpsquest:chararray, hcahpsansdes:chararray, patientsurvstarrat:chararray, patientsurvstarratfn:chararray, hcahpsansper:chararray, hcahpsansperfn:chararray, hcahpslinmeanval:chararray, numofcompsurv:chararray, numofcompsurvfn:chararray, survresprateper:chararray, survresprateperfn:chararray, meastartdate:chararray, meaenddate:chararray, hoslocation:chararray);

CountyRowDel = FILTER HospSurv BY (hcountyname != '');

ReplData = FOREACH CountyRowDel GENERATE Pid, hname, haddress, hcity, hstate, hcode, hcountyname, hphonenum, hcahpsmeasID, hcahpsquest, hcahpsansdes, REPLACE(patientsurvstarrat, 'Not A.*', '0'),patientsurvstarratfn, REPLACE(hcahpsansper, 'Not A.*', '0'), hcahpsansperfn, REPLACE(hcahpslinmeanval, 'Not A.*', '0'), REPLACE(numofcompsurv, 'Not A.*', '0'), numofcompsurvfn, REPLACE(survresprateper, 'Not A.*', '0'), survresprateperfn, meastartdate, meaenddate, hoslocation;

FinalData = ORDER ReplData BY Pid ASC;

STORE FinalData INTO 'hdfs://localhost:54310/data/output77' USING PigStorage(';');

