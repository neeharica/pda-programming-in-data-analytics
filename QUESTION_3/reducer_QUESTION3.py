#!/usr/bin/python

import sys


distKeyAns = {}
distKeyAns1 = {}
distKeyTermAns = {}
distKeyOutput = {}
distTermList = {" quiet ", " medicines ", " communication ", " recommend ", " linear ", " star ",  " always ", " sometimes ", " usually ", " staff "}


for line in sys.stdin:
    data_mapped = line.strip().split(".,")
    if len(data_mapped) != 2:
        # Something has gone wrong. Skip this line.
        continue

    curKey, curData = data_mapped


    #print(inter_data)

    if curKey not in distKeyTermAns:
        distKeyTermAns[curKey] = {}
    for term in distTermList:
        
        if term in curData:
            if term not in distKeyTermAns[curKey]:
                distKeyTermAns[curKey][term] = {}
            distKeyTermAns[curKey][term][curData] = distKeyTermAns[curKey][term].get(curData, 0) + 1

        
        

for key in sorted(distKeyTermAns.keys()):
    term = distKeyTermAns[key]
    for data in sorted(term.keys()):
        countdata = term[data]
        for counts in sorted(countdata.keys()):
            distKeyAns[data] = distKeyAns.get(data, 0) + countdata[counts]
            distKeyAns1[data] = distKeyAns1.get(data, "") +"<==>" + key + "::" + counts



for key in sorted(distKeyAns.keys()):
    
    for key1 in sorted(distKeyAns1.keys()):
        if key1 == key:
            if key not in distKeyOutput:
                distKeyOutput[key] = {}
            
            distKeyOutput[key][distKeyAns[key]] = distKeyOutput[key].get(distKeyAns[key], "") + distKeyAns1[key1]
            

print("HOSPITAL ID, TOTAL TERM")
print("===========  ==========")

for key in sorted(distKeyAns.keys()):
    print "{0},\t{1} ".format(key, distKeyAns[key] )

print("TERM, TERMCOUNT, HOSPITALID :: HOSPITAL NAME :: HCAHPS ANSWERS THAT CONTAIN THE TERM")
print("====  =========  ===================================================================")

for key in sorted(distKeyOutput.keys()):
    term = distKeyOutput[key]
    for data in sorted(term.keys()):
        print "{0},\t{1},\t{2} ".format(key, data, term[data]  )
    

